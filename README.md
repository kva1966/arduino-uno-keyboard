# README 

The TL;DR is this augments [Mitchtech's Arduino USB HID Keyboard hack](http://mitchtech.net/arduino-usb-hid-keyboard/). 

None of the hard work is mine, I simply added some scripts for my own use, and documented things I thought were unclear, and added some template code.

See docs/readme.md for details.

 