# Prereqs

        aptitude install dfu-programmer dfu-util

When using these tools, either root/sudo them as needed. Accessing ports is 
generally privileged.

Firmware files are stored in this project in the `firmware` directory. If no 
directory qualified, assume we are running in that directory.


# HID Firmware for Uno

This readme essentially summarises and clarifies
[Mitchtech's Arduino USB HID Keyboard hack](http://mitchtech.net/arduino-usb-hid-keyboard/).

From the document:

---

NOTE: The Arduino can only be flashed with skectches through the Adruino IDE if 
the Arduino-usbserial.hex bootloader is active. So, to develop a USB HID 
device, the process becomes:

* Flash Arduino-usbserial.hex bootloader with dfu-programmer (erase/flash/reset)
* Plug cycle the Arduino
* Flash firmware sketch using Arduino IDE
* Plug cycle the Arduino
* Flash Arduino-keyboard-0.3.hex bootloader with dfu-programmer (erase/flash/reset)

Test and repeat

---


# Execution

## Step 0: Set the Arduino to DFU mode

This [official Arduino DFU Programming](https://www.arduino.cc/en/Hacking/DFUProgramming8U2) 
shows how to update firmware more generally.

But, in essence to enter DFU mode, short the requisite pins as 
[shown in this picture](https://www.arduino.cc/en/uploads/Hacking/Uno-front-DFU-reset.png)
for about 2-4 seconds.

Executing `dfu-util -l` should now show an entry. 

Example output:

        # dfu-util -l
        dfu-util 0.8

        Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
        Copyright 2010-2014 Tormod Volden and Stefan Schmidt
        This program is Free Software and has ABSOLUTELY NO WARRANTY
        Please report bugs to dfu-util@lists.gnumonks.org

        Found DFU: [03eb:2fef] ver=0000, devnum=32, cfg=1, intf=0, alt=0, name="UNKNOWN", serial="UNKNOWN"


Note that it's worth running `dfu-util` prior to connecting the Arduino, it turns 
out I had a Belkin USB Bluetooth Dongle connected that displayed as running in 
DFU mode!

*DFU Mode Visual Indicator*: On my Uno, there is an on-board orange LED. When 
shorting the pins above, the LED blinks a moment, then turns off. `dfu-util` 
would still be the best check reliable, but the indicator can be useful to know 
when to stop shorting.


## Step 1: Normal Sketch Mode to Upload Programs/Sketches

For the Uno that I have, it's an atmega 16u2 chip. (Just place a the atmega chip on the
Arduino under a decent light source, and see what it says).

        dfu-programmer atmega16u2 erase
        dfu-programmer atmega16u2 flash --debug 1 Arduino-usbserial-uno.hex
        dfu-programmer atmega16u2 reset

Plug-cycling the Arduino should now show the standard serial connection, e.g. 
`ttyACM0: USB ACM device` in `dmesg`.

Create the sketch, and upload to the Arduino -- business as usual, IDE 
compile+upload.



## Step 2: Place in HID Mode

First, do *Step 0* again, we will be using `dfu-programmer` again.

This does *NOT* overwrite sketches, the latter go into a different memory segment
from the firmware. 

Burn in the HID firmware to set into HID mode.

        dfu-programmer atmega16u2 erase
        dfu-programmer atmega16u2 flash --debug 1 Arduino-keyboard-0.3.hex
        dfu-programmer atmega16u2 reset

Plug-cycling the Arduino will now register the Arduino as a USB HID device, e.g.:


        [473652.244829] usb 3-3: new full-speed USB device number 13 using xhci_hcd
        [473652.373801] usb 3-3: New USB device found, idVendor=03eb, idProduct=2042
        [473652.373816] usb 3-3: New USB device strings: Mfr=1, Product=2, SerialNumber=0
        [473652.373818] usb 3-3: Product: Keyboard
        [473652.373820] usb 3-3: Manufacturer: Arduino
        [473652.374072] usb 3-3: ep 0x81 - rounding interval to 64 microframes, ep desc says 80 microframes
        [473652.375270] input: Arduino Keyboard as /devices/pci0000:00/0000:00:140/usb3/-3/3-3:1.0/0003:03EB:2042.0009/input/input30
        [473652.428910] hid-generic 0003:03EB:2042.0009: input,hidraw4: USB HID v1.11 
        Keyboard [Arduino Keyboard] on usb-0000:00:14.0-3/input0


# Examples

Looking at the code snippets 
(also in http://mitchtech.net/arduino-usb-hid-keyboard/), 
it seems that HID mode simply writes to the (USB) serial port, using the HID 
protocol.

        https://gist.github.com/mitchtech/2865205#file-usb_hid_random-ino
        https://gist.github.com/mitchtech/2865200#file-usb_hid_volume-ino
        https://gist.github.com/mitchtech/2865219#file-usb_hid_cut_copy_paste-ino


# Key Codes

https://github.com/buaabyl/vmulti/blob/master/doc/USB%20HID%20Usage%20Tables%201.1.pdf

Page 53, added to docs/

https://www.win.tue.nl/~aeb/linux/kbd/scancodes-14.html

Also stored in docs/ folder.

http://www.freebsddiary.org/APC/usb_hid_usages.php

However, note that with modifiers, it's not as straightforward.

Various useful comments in page:

---

buf[0] is for the modifier keys such as control, shift and alt. You can simulate 
multiple modifier keys simultaneously pressed by making 

        buf[0]=KEY_LEFT_CTRL+KEY_LEFT_ALT;
        Then buf[2]=KEY_C;

---

Read the:

        http://www.usb.org/developers/devclass_docs/Hut1_11.pdf

From page 53 you can find the desiered ID’s for Keys you want to use.

Her you can see a sketch I’ve made for an Arduino project, to change XPNDR code 
for my flightsimulator: 

http://www.mycockpit.org/forums/arduino-cards-link2fs/26081-transponder-xpndr.html

TX to MICHAEL for this input :)        

---

It seems to be explained at the following link. I was tearing my hair out too.

http://www.circuitsathome.com/mcu/lightweight-usb-host-part-6-hid

It seems to be something called the book keyboard report.


sorry, about should be “boot keyboard report”. Here is a link to a header file 
also.

https://github.com/felis/lightweight-usb-host/blob/master/HID.h

(look for `BOOT_KBD_REPORT`)

----


Curtdissel
October 17, 2013

If I desire to press three keys at once like alt-control-delete.
what is the best way to handle that?
reply

    marc
    February 15, 2015

    Serial.write(228); //send control
    Serial.write(230); //send ALT
    Serial.write(99); //send DELETE
    works fine for me

---


The link to the PDF file is dead. You can download a copy of the HID keyboard 
mappings here: https://github.com/buaabyl/vmulti/blob/master/doc/USB%20HID%20Usage%20Tables%201.1.pdf

Click on raw data. – Digitol

Page 53

-------

