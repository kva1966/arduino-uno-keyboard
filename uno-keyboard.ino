const int SW1_PIN = 2;
const int LED_PIN = 12;

const int MODIFIER_INDEX = 0;
const int REGULARKEY_INDEX = 2;
const int EMPTY_KEY = 0;
const int SERIAL_BUFF_LEN = 8;

const int DEBOUNCE_MS = 50;
const int INIT_DELAY_MS = 200;

struct SwitchState {
  int current;
  int previous;
};

SwitchState switchState = { LOW, LOW };


/* GLOBAL: Keyboard report buffer */
uint8_t buf[8] = { 
  EMPTY_KEY
};


void setup() {
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  pinMode(SW1_PIN, INPUT);
  
  delay(INIT_DELAY_MS);
}

void loop() {
  onSwitchEvents(
    &onSwitchPressed,
    &onSwitchReleased
  );
}


//
// UTIL 
//

void onSwitchPressed() {
  digitalWrite(LED_PIN, HIGH);

  // "b" -- note that this is the raw keycode, when displayed on an application,
  // it will be mapped via software, e.g. dvorak mappings: b -> x
  sendKey(5);
  releaseKey();
}

void onSwitchReleased() {
  digitalWrite(LED_PIN, LOW);
}

void sendKey(uint8_t regularCode) {
  sendKeyMod(EMPTY_KEY, regularCode);
}

void sendKeyMod(uint8_t modifierCode, uint8_t regularCode) {
  buf[MODIFIER_INDEX] = modifierCode;
  buf[REGULARKEY_INDEX] = regularCode;
  Serial.write(buf, SERIAL_BUFF_LEN);
}

void releaseKey() 
{
  buf[MODIFIER_INDEX] = EMPTY_KEY;
  buf[REGULARKEY_INDEX] = EMPTY_KEY;
  Serial.write(buf, SERIAL_BUFF_LEN);
}


void onSwitchEvents(void (*handlePressed)(void), void (*handleNotPressed)(void)) {
  switchState.current = digitalRead(SW1_PIN);

  if (switchState.current != switchState.previous) {
    if (switchState.current == HIGH) {
      (*handlePressed)();
    } else {
      (*handleNotPressed)();
    }

    switchState.previous = switchState.current;
    
    delay(DEBOUNCE_MS);
  }
}


